import { GraphQLModule } from '@nestjs/graphql';
import { Module } from '@nestjs/common';

const GQL = GraphQLModule.forRoot({
  typePaths: ['./**/*.graphql'],
  context: ({ req }) => ({ req }),
  introspection: true,
  playground: process.env.APP_ENV === 'development',
  debug: process.env.APP_ENV === 'development',
  path: '/',
});

@Module({
  imports: [GQL],
  exports: [GQL],
})
export class GraphqlModule {}
