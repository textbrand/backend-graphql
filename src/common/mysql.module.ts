import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';

const TYPEORM = TypeOrmModule.forRoot({
  type: 'mysql',
  host: process.env.MSQL_HOST,
  port: 3306,
  username: process.env.MSQL_USERNAME,
  password: process.env.MSQL_PASSWORD,
  database: process.env.MSQL_DATABASE,
  entities: [join(__dirname, '..', '/modules/**/*.entity{.ts,.js}')],
  synchronize: true,
});

@Module({
  imports: [TYPEORM],
  exports: [TYPEORM],
})
export class MysqlModule {}
