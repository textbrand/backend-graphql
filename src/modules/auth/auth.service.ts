import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './jwt-payload.interface';
import { LoginDTO } from './auth.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async signIn(payload: JwtPayload): Promise<string> {
    // In the real-world app you shouldn't expose this method publicly
    // instead, return a token once you verify user credentials
    return await this.jwtService.sign(payload);
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    return await this.usersService.findOneByPayload(payload);
  }

  async login(login: LoginDTO) {
    const { email, password } = login;
    const user = await this.usersService.findByEmail(email);

    let accessToken;
    const match = await bcrypt.compare(password, user.password);
    if (match) {
      accessToken = await this.signIn(await this.getPayload(user.id));
    } else {
      throw new UnauthorizedException('Invalid credentials');
    }

    return { token: accessToken, user };
  }

  async getPayload(userId: any): Promise<JwtPayload> {
    const user = await this.usersService.findById(userId);
    return {
      email: user.email,
    };
  }
}
