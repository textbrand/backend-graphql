import { LoginDTO } from './auth.dto';
import { AuthService } from './auth.service';
import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';

@Resolver('Auth')
export class AuthResolver {
  constructor(private readonly authService: AuthService) {}

  @Mutation()
  async userLogin(@Args('login') login: LoginDTO) {
    return await this.authService.login(login);
  }
}
