import { SenderNamesModule } from './sender-names/sender-names.module';
import { MessagesModule } from './messages/messages.module';
import { CreditsModule } from './credits/credits.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { GraphqlModule } from './../common/graphql.module';
import { MysqlModule } from './../common/mysql.module';
import { Module } from '@nestjs/common';

const MODULES = [
  MysqlModule,
  GraphqlModule,
  UsersModule,
  AuthModule,
  CreditsModule,
  MessagesModule,
  SenderNamesModule,
];

@Module({
  imports: [...MODULES],
  exports: [...MODULES],
})
export class CoreModule {}
