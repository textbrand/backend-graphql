import { MessageTemplates } from './message-templates.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from './../../../base/base.service';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class MessageTemplatesService extends BaseService {
  constructor(
    @InjectRepository(MessageTemplates)
    private readonly repo: Repository<MessageTemplates>,
  ) {
    super(repo);
  }
}
