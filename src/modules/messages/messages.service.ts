import { SentItemsService } from './sent-items/sent-items.service';
import { BatchesService } from './batches/batches.service';
import { MessageDTO } from './messages.dto';
import { Injectable } from '@nestjs/common';
import { CreditsService } from '../credits/credits.service';

@Injectable()
export class MessagesService {
  validMobileNo: string[];

  constructor(
    private readonly creditsService: CreditsService,
    private batchesService: BatchesService,
    public sentItemsService: SentItemsService,
  ) {}

  async queueMessage(messageDetails: MessageDTO, senderId: string) {
    const { receiverNo, message, senderNameId } = messageDetails;
    const totalAvailableCredits = await this.creditsService.calculatetAvailableCredits(
      senderId,
    );
    let returnMessage: string = 'Unable to send messages this time.';
    let queuedCounter = 0;

    const mobileNoCount = receiverNo.length;
    if (totalAvailableCredits < mobileNoCount) {
      returnMessage = `Sending ${mobileNoCount} message(s) failed. You only have ${totalAvailableCredits} remaining credit balance.`;
      return { status: false, message: returnMessage };
    }

    this.setValidMobileNo(receiverNo);

    if (this.validMobileNo.length > 0) {
      const { id: batchId } = await this.batchesService.create({ message });

      if (batchId) {
        for (const mobileNo of this.validMobileNo) {
          const sentItemCreated = await this.sentItemsService.create({
            senderId,
            senderNameId,
            batchId,
            receiverNo: mobileNo,
          });

          if (sentItemCreated) {
            queuedCounter++;
          }
        }

        if (!!queuedCounter) {
          returnMessage = 'Message sent successfully.';
        }
      }
    }

    await this.sentItemsService.processPendingMessage();
    return { status: !!queuedCounter, message: returnMessage };
  }

  private setValidMobileNo(mobileNos: string[]) {
    this.validMobileNo = [];
    mobileNos.forEach(mobileNo => {
      if (this.isValidMobileNo(mobileNo)) {
        this.validMobileNo.push(mobileNo);
      }
    });
  }

  private isValidMobileNo(mobileNo: string): boolean {
    const mobileNoPrefix = mobileNo.slice(0, 2);
    const mobileNoLength = mobileNo.length === 12;

    return mobileNoPrefix && mobileNoLength;
  }
}
