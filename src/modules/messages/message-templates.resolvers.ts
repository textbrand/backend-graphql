import { User } from './../../decorators/user.decorator';
import { MessageTemplatesService } from './message-templates/message-templates.service';
import { GqlAuthGuard } from './../../guards/gql-jwt-auth.guard';
import {
  Resolver,
  Query,
  Args,
  Mutation,
  ResolveProperty,
  Parent,
} from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { Users } from '../users/users.entity';

@UseGuards(GqlAuthGuard)
@Resolver('MessageTemplate')
export class MessageTemplatesResolver {
  constructor(
    private readonly messageTemplatesService: MessageTemplatesService,
    private readonly usersService: UsersService,
  ) {}

  @Query()
  async allMessageTemplates(@Args('userId') userId?: string) {
    return await this.messageTemplatesService.findAll();
  }

  @Mutation()
  async createMessageTemplate(@Args('input') input: any, @User() user: Users) {
    return await this.messageTemplatesService.create({
      ...input,
      userId: user.id,
    });
  }

  @Mutation()
  async deleteMessageTemplate(
    @Args('messageTemplateId') messageTemplateId: string,
  ) {
    return await this.messageTemplatesService.delete(messageTemplateId);
  }

  @Mutation()
  async updateMessageTemplate(
    @Args('messageTemplateId') messageTemplateId: string,
    @Args('input') input: any,
  ) {
    return this.messageTemplatesService.update(messageTemplateId, input);
  }

  @ResolveProperty('user')
  async getUser(@Parent() { userId }) {
    return await this.usersService.findById(userId);
  }
}
