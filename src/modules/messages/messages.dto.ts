export interface MessageDTO {
  senderNameId: string;
  receiverNo: [string];
  message: string;
}

export interface BatchDTO {
  id: string;
  message: string;
}

export interface SentItemDTO {
  senderId: string;
  senderNameId: string;
  batchId: string;
  receiverNo: string;
}

export interface MessageTemplateDTO {
  id: string;
  name: string;
  message: string;
}
