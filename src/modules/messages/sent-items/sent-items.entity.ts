import { SenderNames } from './../../sender-names/sender-names.entity';
import { Users } from './../../users/users.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  AfterLoad,
} from 'typeorm';
import { Batches } from '../batches/batches.entity';

export enum MessageStatus {
  Pending = 'Pending',
  Failed = 'Failed',
  Sent = 'Sent',
}

@Entity()
export class SentItems {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, sender => sender.sentItems)
  sender: Users;

  @Column('int')
  senderId: number;

  @ManyToOne(type => Batches, batch => batch.sentItems, {
    eager: true,
  })
  batch: Batches;

  @Column('int')
  batchId: number;

  @ManyToOne(type => SenderNames, senderName => senderName.sentItems, {
    eager: true,
  })
  senderName: SenderNames;

  @Column('int')
  senderNameId: number;

  @Column({ length: 15 })
  receiverNo: string;

  @Column({ type: 'enum', enum: MessageStatus, default: MessageStatus.Pending })
  status: MessageStatus;

  @Column({ type: 'boolean', default: false })
  processing: boolean;

  @Column()
  response: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  senderNameUsed: string;
  @AfterLoad()
  setSenderNameUsed() {
    this.senderNameUsed = this.senderName.name;
  }

  message: string;
  @AfterLoad()
  setMessage() {
    this.message = this.batch.message;
  }
}
