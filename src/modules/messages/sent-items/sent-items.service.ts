import { SMSDefaults } from './../../../configs/sms.configs';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseService } from './../../../base/base.service';
import { Injectable, HttpService } from '@nestjs/common';
import { SentItems, MessageStatus } from './sent-items.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SentItemsService extends BaseService {
  constructor(
    @InjectRepository(SentItems)
    private readonly repo: Repository<SentItems>,
    private readonly httpService: HttpService,
  ) {
    super(repo);
  }

  async setSentStatus(sentItemId, response?: string) {
    await this.repo.update(sentItemId, {
      status: MessageStatus.Sent,
      processing: false,
      response,
    });
  }

  async setFailedStatus(sentItemId, response?: string) {
    await this.update(sentItemId, {
      status: MessageStatus.Failed,
      processing: false,
      response,
    });
  }

  async setMessagesToProcessing(sentItems) {
    let sentItemIds: string[] = [];

    for (const sentItem of sentItems) {
      sentItemIds = [...sentItemIds, sentItem.id];
    }

    await this.repo.update(sentItemIds, { processing: true });
  }

  async processPendingMessage() {
    const sentItems = await this.findAll({
      status: MessageStatus.Pending,
      processing: false,
    });

    if (sentItems.length > 0) {
      await this.setMessagesToProcessing(sentItems);
      for (const sentItem of sentItems) {
        const { receiverNo, id: sentItemId } = sentItem;
        await this.sendSMS(sentItemId, {
          dstno: receiverNo,
          msg: sentItem.batch.message,
          sendid: sentItem.senderName.name,
        });
      }
    }
  }

  async sendSMS(sentItemId, smsDetails) {
    const params = { ...SMSDefaults, ...smsDetails };
    await this.httpService
      .get(process.env.SMS_URL, { params })
      .subscribe(async ({ data }) => {
        const response = data.split('=');
        const statusCode = response[0].trim();

        if (statusCode === '2000') {
          await this.setSentStatus(sentItemId, data);
        } else {
          await this.setFailedStatus(sentItemId, data);
        }
      });
  }
}
