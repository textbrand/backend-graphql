import { User } from './../../decorators/user.decorator';
import { MessageDTO } from './messages.dto';
import { MessagesService } from './messages.service';
import { GqlAuthGuard } from './../../guards/gql-jwt-auth.guard';
import { Resolver, Mutation, Args, Query, Subscription } from '@nestjs/graphql';
import { UseGuards, UnauthorizedException } from '@nestjs/common';

@UseGuards(GqlAuthGuard)
@Resolver('Message')
export class MessagesResolver {
  constructor(private messagesService: MessagesService) {}

  @Mutation()
  async sendMessage(@Args('input') input: MessageDTO, @User() user) {
    if (user) {
      return await this.messagesService.queueMessage(input, user.id);
    } else {
      throw new UnauthorizedException('Unauthorized user');
    }
  }

  @Query()
  async allSentMessages(@Args('input') input: MessageDTO, @User() user) {
    const condition = input ? input : { senderId: user.id };
    return await this.messagesService.sentItemsService.findAll({
      ...condition,
      order: { createdAt: 'DESC' },
    });
  }
}
