import { InjectRepository } from '@nestjs/typeorm';
import { Batches } from './batches.entity';
import { BaseService } from './../../../base/base.service';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class BatchesService extends BaseService {
  constructor(
    @InjectRepository(Batches)
    private readonly repo: Repository<Batches>,
  ) {
    super(repo);
  }
}
