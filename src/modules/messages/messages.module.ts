import { CreditsModule } from './../credits/credits.module';
import { MessageTemplatesResolver } from './message-templates.resolvers';
import { MessagesResolver } from './messages.resolvers';
import { MessageTemplates } from './message-templates/message-templates.entity';
import { Batches } from './batches/batches.entity';
import { SentItems } from './sent-items/sent-items.entity';
import { Module, HttpModule, forwardRef } from '@nestjs/common';
import { SentItemsService } from './sent-items/sent-items.service';
import { BatchesService } from './batches/batches.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { MessagesService } from './messages.service';
import { MessageTemplatesService } from './message-templates/message-templates.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([SentItems, Batches, MessageTemplates]),
    HttpModule,
    UsersModule,
    forwardRef(() => CreditsModule),
  ],
  providers: [
    SentItemsService,
    BatchesService,
    MessagesResolver,
    MessagesService,
    MessageTemplatesService,
    MessageTemplatesResolver,
  ],
  exports: [SentItemsService],
})
export class MessagesModule {}
