import { SenderNamesResolver } from './sender-names.resolvers';
import { UsersModule } from './../users/users.module';
import { UsersService } from './../users/users.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SenderNamesService } from './sender-names.service';
import { SenderNames } from './sender-names.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SenderNames]), UsersModule],
  providers: [SenderNamesService, SenderNamesResolver],
})
export class SenderNamesModule {}
