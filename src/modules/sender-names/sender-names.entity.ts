import { Users } from './../users/users.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { SentItems } from '../messages/sent-items/sent-items.entity';

export enum Status {
  Validating = 'Validating',
  Active = 'Active',
  Declined = 'Declined',
}

@Entity()
export class SenderNames {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Users, user => user.senderName)
  user: Users;

  @Column('int')
  userId: number;

  @Column({ length: 15, unique: true })
  name: string;

  @Column()
  sampleMessage: string;

  @Column({ type: 'enum', enum: Status, default: Status.Validating })
  status: Status;

  @Column({ type: 'boolean', default: true })
  paid: boolean;

  @Column({ default: 'NA' })
  nextRenewal: string;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  @OneToMany(type => SentItems, sentItem => sentItem.senderName)
  sentItems: SentItems[];
}
