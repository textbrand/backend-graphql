import { BaseService } from '../../base/base.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { SenderNames } from './sender-names.entity';

@Injectable()
export class SenderNamesService extends BaseService {
  constructor(
    @InjectRepository(SenderNames)
    private readonly repo: Repository<SenderNames>,
  ) {
    super(repo);
  }
}
