import { Status } from './sender-names.entity';
export interface CreateSenderName {
  name: string;
  sampleMessage: string;
}

export interface UpdateSenderName {
  userId: string;
  name: string;
  sampleName: string;
  status: Status;
  paid: boolean;
  newtRenewal: string;
}
