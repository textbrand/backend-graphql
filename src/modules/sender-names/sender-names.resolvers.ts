import { User } from './../../decorators/user.decorator';
import { GqlAuthGuard } from './../../guards/gql-jwt-auth.guard';
import {
  Resolver,
  Query,
  Args,
  Mutation,
  ResolveProperty,
  Parent,
} from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { SenderNamesService } from './sender-names.service';
import { UsersService } from '../users/users.service';
import { CreateSenderName, UpdateSenderName } from './sender-names.dto';
import { Users } from '../users/users.entity';

@UseGuards(GqlAuthGuard)
@Resolver('SenderName')
export class SenderNamesResolver {
  constructor(
    private readonly senderNamesService: SenderNamesService,
    private readonly usersService: UsersService,
  ) {}

  @Query()
  async allSenderNames() {
    return await this.senderNamesService.findAll();
  }

  @Mutation()
  async createSenderName(
    @Args('input') input: CreateSenderName,
    @User() user: Users,
  ) {
    return await this.senderNamesService.create({ ...input, userId: user.id });
  }

  @Mutation()
  async deleteSenderName(@Args('senderNameId') senderNameId: string) {
    return await this.senderNamesService.delete(senderNameId);
  }

  @Mutation()
  async updateSenderName(
    @Args('senderNameId') senderNameId: string,
    @Args('input') input: UpdateSenderName,
  ) {
    return this.senderNamesService.update(senderNameId, input);
  }

  @ResolveProperty('user')
  async getUser(@Parent() { userId }) {
    return await this.usersService.findById(userId);
  }
}
