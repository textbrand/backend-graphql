import {
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  AfterLoad,
} from 'typeorm';
import { Users } from '../users/users.entity';

export enum CreditStatus {
  Pending = 'Pending',
  Declined = 'Declined',
  Approved = 'Approved',
}

export enum CreditMethod {
  'GCash',
  'Coins.ph',
  'BPI - Brank Transfer',
  'UnionBrank - Bank Transfer',
}

@Entity()
export class Credits {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('int')
  userId: number;

  @ManyToOne(type => Users, user => user.credits, { eager: true })
  user: Users;

  @Column('double')
  amount: number;

  @Column('double')
  totalPrice: number;

  @Column({ type: 'enum', enum: CreditMethod })
  method: CreditMethod;

  @Column({ length: 150 })
  identifier: string;

  @Column({ type: 'enum', enum: CreditStatus, default: CreditStatus.Pending })
  status: CreditStatus;

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

  protected userName: string;
  @AfterLoad()
  setUserName() {
    this.userName = this.user.fullName;
  }
}
