import * as dto from './credits.dto';
import { GqlAuthGuard } from './../../guards/gql-jwt-auth.guard';
import { User } from './../../decorators/user.decorator';
import { CreditsService } from './credits.service';
import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';

@UseGuards(GqlAuthGuard)
@Resolver('Credit')
export class CreditsResolver {
  constructor(private creditsService: CreditsService) {}

  @Query()
  async allCredits(@Args('input') input, @User() user) {
    const condition = input ? input : { userId: user.id };
    return await this.creditsService.findAll(condition);
  }

  @Query()
  async totalAvailableCredits(@Args('userId') userId, @User() user) {
    return await this.creditsService.calculatetAvailableCredits(
      userId ? userId : user.id,
    );
  }

  @Mutation()
  async createCredit(@Args('input') input: dto.CreditDTO, @User() user) {
    input.totalPrice = input.amount * 2;
    const { id } = await this.creditsService.create({
      ...input,
      userId: user.id,
    });
    return await this.creditsService.findById(id);
  }

  @Mutation()
  async deleteCredit(@Args('creditId') creditId: string) {
    return await this.creditsService.delete(creditId);
  }

  @Mutation()
  async updateCredit(
    @Args('creditId') creditId: string,
    @Args('input') input: dto.CreditUpdateDTO,
  ) {
    return this.creditsService.update(creditId, input);
  }
}
