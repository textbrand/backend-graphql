import { CreditsResolver } from './credits.resolvers';
import { Module, forwardRef } from '@nestjs/common';
import { CreditsService } from './credits.service';
import { Credits } from './credits.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MessagesModule } from '../messages/messages.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Credits]),
    forwardRef(() => MessagesModule),
  ],
  providers: [CreditsService, CreditsResolver],
  exports: [CreditsService],
})
export class CreditsModule {}
