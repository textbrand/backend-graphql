import { CreditMethod, CreditStatus } from './credits.entity';
export interface CreditDTO {
  id?: string;
  userName?: string;
  amount: number;
  totalPrice: number;
  method: CreditMethod;
  identifier: string;
  status?: CreditStatus;
  createAt?: string;
}

export interface CreditUpdateDTO {
  amount: number;
  totalPrice: number;
  method: CreditMethod;
  identifier: string;
  status: CreditStatus;
}
