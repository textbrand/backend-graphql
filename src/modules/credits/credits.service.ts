import { SentItemsService } from './../messages/sent-items/sent-items.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Credits, CreditStatus } from './credits.entity';
import { BaseService } from './../../base/base.service';
import { Injectable, forwardRef, Inject } from '@nestjs/common';
import { Repository } from 'typeorm';

@Injectable()
export class CreditsService extends BaseService {
  constructor(
    @Inject(forwardRef(() => SentItemsService))
    private readonly sentItemService: SentItemsService,
    @InjectRepository(Credits)
    private readonly repo: Repository<Credits>,
  ) {
    super(repo);
  }

  async calculatetAvailableCredits(userId: string): Promise<number> {
    const allCredits = await this.findAll({
      userId,
      status: CreditStatus.Approved,
    });
    const sentItems = await this.sentItemService.findAll({ senderId: userId });
    const totalSentItems = sentItems.filter(
      sentItem => sentItem.status !== 'Failed',
    );

    const totalCredits = allCredits.reduce(
      (acc, credit) => acc + credit.amount,
      0,
    );

    return totalCredits - totalSentItems.length;
  }
}
