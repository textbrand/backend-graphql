import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import * as dto from './users.dto';
import { UsersService } from './users.service';

@Resolver('Users')
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Query()
  async allUsers() {
    return await this.usersService.findAll();
  }

  @Mutation()
  async createUser(@Args('input') newUser: dto.CreateDTO) {
    return await this.usersService.create(newUser);
  }

  @Mutation()
  async deleteUser(@Args('userId') userId: string) {
    return await this.usersService.delete(userId);
  }

  @Mutation()
  async updateUser(
    @Args('userId') userId: string,
    @Args('input') input: dto.UpdateDTO,
  ) {
    return this.usersService.update(userId, input);
  }
}
