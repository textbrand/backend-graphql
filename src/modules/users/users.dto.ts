export interface CreateDTO {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
}

export interface UpdateDTO {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
}
