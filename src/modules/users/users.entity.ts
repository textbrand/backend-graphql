import {
  Entity,
  Column,
  BeforeInsert,
  BeforeUpdate,
  AfterLoad,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Credits } from './../credits/credits.entity';
import { SenderNames } from '../sender-names/sender-names.entity';
import { SentItems } from '../messages/sent-items/sent-items.entity';
import { MessageTemplates } from '../messages/message-templates/message-templates.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  firstname: string;

  @Column({ length: 50 })
  lastname: string;

  @Column({ length: 50 })
  email: string;

  @Column({ length: 15 })
  mobileNo: string;

  @Column()
  password: string;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (this.password) {
      this.password = await bcrypt.hash(this.password, 10);
    }
  }

  fullName: string;
  @AfterLoad()
  setFullName() {
    this.fullName = `${this.firstname} ${this.lastname}`;
  }

  @OneToMany(type => SenderNames, semderName => semderName.user)
  senderName: SenderNames[];

  @OneToMany(type => Credits, credit => credit.user)
  credits: Credits[];

  @OneToMany(type => SentItems, sentItem => sentItem.sender)
  sentItems: SentItems[];

  @OneToMany(
    type => MessageTemplates,
    messageTemplates => messageTemplates.user,
  )
  messageTemplates: MessageTemplates[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}
