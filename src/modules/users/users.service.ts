import { JwtPayload } from './../auth/jwt-payload.interface';
import { BaseService } from './../../base/base.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Users } from './users.entity';

@Injectable()
export class UsersService extends BaseService {
  constructor(
    @InjectRepository(Users)
    private readonly repo: Repository<Users>,
  ) {
    super(repo);
  }

  async findOneByPayload(payload: JwtPayload) {
    return await this.repo.findOne({ email: payload.email });
  }

  async findByEmail(email: string) {
    const user = await this.repo.findOne({ email });
    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }

    return user;
  }
}
