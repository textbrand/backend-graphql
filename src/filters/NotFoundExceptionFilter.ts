import {
  HttpException,
  ArgumentsHost,
  NotFoundException,
  Catch,
  ExceptionFilter,
} from '@nestjs/common';
import * as path from 'path';

@Catch(NotFoundException)
export class NotFoundExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    response.redirect('/');
  }
}
