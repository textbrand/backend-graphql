import 'dotenv/config';
import { NotFoundExceptionFilter } from './filters/NotFoundExceptionFilter';
import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';

const PORT = process.env.PORT || 8080;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.useGlobalFilters(new NotFoundExceptionFilter());

  await app.listen(PORT);
  Logger.warn(`Running at port ${PORT}...`);
}
bootstrap();
