export const SMSDefaults = {
  un: process.env.SMS_USERNAME,
  pwd: process.env.SMS_PASSWORD,
  dstno: '',
  msg: '',
  type: '1',
  agreedterm: 'Yes',
  sendid: '',
};
