FROM node:10-alpine
RUN mkdir -p /opt/app
WORKDIR /opt/app
COPY . /opt/app

ENV PORT=80
# --no-cache: download package index on-the-fly, no need to cleanup afterwards
# --virtual: bundle packages, remove whole bundle at once, when done
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++ \
    && npm install --g --production && npm audit fix --force && npm install bcrypt\
    && apk del build-dependencies

RUN npm run build
COPY dist /opt/app/
COPY public /opt/app/

CMD [ "npm", "run", "start:prod" ]
